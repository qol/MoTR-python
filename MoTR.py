import numpy
import pandas
from sklearn import linear_model
import matplotlib.pyplot as plt
import random
import time
from scipy import stats

#################################################

#THE ONLY PART OF THE CODE THAT MUST BE CHECKED/CHANGED BEFORE EVERY RUN

def constants():
    constants.DATA_INPUT_CSV = ""
    constants.NUM_ROWS_TO_READ = 0

    constants.IDV_COLUMN_NAME = ""
    constants.DV_COLUMN_NAME = ""

    constants.TOTAL_columns = [constants.IDV_COLUMN_NAME, constants.DV_COLUMN_NAME, '']
    constants.remove_from_CONTROLS = [] # these columns won't be included in the controls
    constants.CATEGORICAL_variables = []  # all interactions will be autom. added to the list of Categorical variables

    constants.force_minimum_CATEGORICAL_types = False #set True to enable a minimum number of categories for every categorical variable
                                                      # (e.g., 12 months even if data has only 8 - 0's for every month not present)
    constants.CATEGORICAL_maxtypes = [0] # still used even if "force_minimum_CATEGORICAL_types" is True
    constants.INTERACTIONS = []  # [name, name, maxtypes] || at least one must be binary (0/1) || any non-binary must be categorical

    constants.LAGS_TO_SIMULATE = [1] #will repeat the run for every lag inside this list
    constants.N_DAYS_LAG_ON_DV = 0  # DO NOT CHANGE THIS

    constants.BINARY_CUTTINGPOINT = 0

    constants.N_REPETITIONS = 10000
    constants.N_REPETITIONS_MIN = 1000
    constants.STOPPING_VALUE = 0.5
    constants.SEED_RANDOM = 1

    constants.NOISE_ON_SIMULATIONS = True
    constants.SAVE_GRAPH = True
    constants.LOG10_BASED = True
    constants.SCIENTIFIC_NOTATION = False
    constants.SAVE_CSV_WITH_SIMULATED_DATA = True

    constants.FOLDER_OUTPUT = ""
    # name of type " [type] _ K [hypothesis] _ [variation, e.g., MONTH] _ [days of lag] lag - "

    # edit also function "update_constants" below
    constants.PREFIX_NAMES_OUTPUT = str(constants.N_DAYS_LAG_ON_DV)+"lag-"
    # ---

    constants.NAME_OUTPUT_TXT = "results.txt"
    constants.NAME_OUTPUT_GRAPH = "results_graph.png"
    constants.NAME_OUTPUT_GRAPH_stopping = "stopping_graph.png"
    constants.NAME_OUTPUT_CSV = "simulated_data.csv"
    constants.NAME_OUTPUT_CSV_2 = "stopping_condition.csv"

    #NO NEED TO CHANGE THE 5 LINES BELOW
    constants.PATH_OUTPUT_TXT = constants.FOLDER_OUTPUT + "/" + constants.PREFIX_NAMES_OUTPUT + constants.NAME_OUTPUT_TXT
    constants.PATH_OUTPUT_GRAPH = constants.FOLDER_OUTPUT + "/" + constants.PREFIX_NAMES_OUTPUT + constants.NAME_OUTPUT_GRAPH
    constants.PATH_OUTPUT_GRAPH_stopping = constants.FOLDER_OUTPUT + "/" + constants.PREFIX_NAMES_OUTPUT + constants.NAME_OUTPUT_GRAPH_stopping
    constants.PATH_OUTPUT_CSV = constants.FOLDER_OUTPUT + "/" + constants.PREFIX_NAMES_OUTPUT + constants.NAME_OUTPUT_CSV
    constants.PATH_OUTPUT_CSV_2 = constants.FOLDER_OUTPUT + "/" + constants.PREFIX_NAMES_OUTPUT + constants.NAME_OUTPUT_CSV_2

#################################################

def update_constants():
    constants.PREFIX_NAMES_OUTPUT = str(constants.N_DAYS_LAG_ON_DV)+"lag-"

    constants.PATH_OUTPUT_TXT = constants.FOLDER_OUTPUT + "/" + constants.PREFIX_NAMES_OUTPUT + constants.NAME_OUTPUT_TXT
    constants.PATH_OUTPUT_GRAPH = constants.FOLDER_OUTPUT + "/" + constants.PREFIX_NAMES_OUTPUT + constants.NAME_OUTPUT_GRAPH
    constants.PATH_OUTPUT_GRAPH_stopping = constants.FOLDER_OUTPUT + "/" + constants.PREFIX_NAMES_OUTPUT + constants.NAME_OUTPUT_GRAPH_stopping
    constants.PATH_OUTPUT_CSV = constants.FOLDER_OUTPUT + "/" + constants.PREFIX_NAMES_OUTPUT + constants.NAME_OUTPUT_CSV
    constants.PATH_OUTPUT_CSV_2 = constants.FOLDER_OUTPUT + "/" + constants.PREFIX_NAMES_OUTPUT + constants.NAME_OUTPUT_CSV_2

#DO NOT CHANGE ANYTHING BELOW THIS LINE

#to print all the lines and columns from the dataframe
pandas.set_option("display.max_rows", None, "display.max_columns", None)

def get_DV_with_lags(DV):
    DV_local = DV.copy()
    DV_previous_days = {}
    for i in range(1, constants.N_DAYS_LAG_ON_DV +1):
        copy = DV_local.copy()[(constants.N_DAYS_LAG_ON_DV-i):-i]
        DV_previous_days[-i] = copy
    return DV_previous_days

def remove_empty_cells_mean(input):
    #apply a mean method in case of just one empty space
    first_index = input.index.start
    for i in range(first_index, len(input)+first_index):
        if numpy.isnan(input[i]):
            if (i-1)>first_index:
                input.at[i] = ((input[i-1] + input[i+1]) / 2)
            else:
                print("\nERROR: Using a lag of {} days, the first day is the #{}, which has missing data. Please choose other lag, such the first day has no empty spaces.".format( str(constants.N_DAYS_LAG_ON_DV), str(i+1) ))
                print("Ending the running.")
                exit()

def remove_empty_cells(input):
    #apply interpolate method, so it can be applied with multiple empty spaces at once
    first_index = input.index.start
    input = input.interpolate()

    if numpy.isnan(input[first_index]):
        print(
            "\nERROR: Using a lag of {} days, the first day is the #{}, which has missing data. Please choose other lag, such the first day has no empty spaces.".format(
                str(constants.N_DAYS_LAG_ON_DV), str(input[constants.N_DAYS_LAG_ON_DV])))
        print("Ending.")
        exit()

    return input

def idv_to_binary(input):
    for i in range(constants.N_DAYS_LAG_ON_DV, constants.NUM_ROWS_TO_READ):
        if input[i] >= constants.BINARY_CUTTINGPOINT:
            input.at[i] = 1
        else:
            input.at[i] = 0

def set_Controls_list():
    CONTROLS = constants.TOTAL_columns.copy()
    CONTROLS.remove(constants.IDV_COLUMN_NAME)
    CONTROLS.remove(constants.DV_COLUMN_NAME)
    return CONTROLS

def get_DV_previous_for_DataFrame(input, keys):
    Columns = []
    Data = []
    for key in reversed(keys):
        Columns.append(str(constants.DV_COLUMN_NAME)+str(key))
        Data.append(input[key])
    return [Columns, Data]

def get_control_values(data_input, CONTROLS):
    control_values = []
    for i in range(0, len(CONTROLS)):
        data = data_input[CONTROLS[i]][constants.N_DAYS_LAG_ON_DV:constants.NUM_ROWS_TO_READ]
        data = remove_empty_cells(data)
        data_without_empty_cells = data
        control_values.append(data_without_empty_cells)
    return control_values

def generate_data_interactions(input):
    input_local = input.copy()
    CONTROLS = 0
    for element in constants.INTERACTIONS:
        A = input_local[element[0]]
        B = input_local[element[1]]
        if (element[2]>2):
            #this means that we need to convert the non-binary to the range 1 - maxtypes+1
            if element[0] in constants.CATEGORICAL_variables:
                #case A
                maxtypes_element = constants.CATEGORICAL_maxtypes[ constants.CATEGORICAL_variables.index(element[0]) ]
                if maxtypes_element > 2:
                    A = list(numpy.array(A+1))
            if element[1] in constants.CATEGORICAL_variables:
                #case B
                maxtypes_element = constants.CATEGORICAL_maxtypes[ constants.CATEGORICAL_variables.index(element[1]) ]
                if maxtypes_element > 2:
                    B = list(numpy.array(B+1))
        C = A*B
        name_new_column = str(element[0]) + "*" + str(element[1])
        input_local[name_new_column] = C
        constants.TOTAL_columns.append(name_new_column)
        CONTROLS = set_Controls_list()
        constants.CATEGORICAL_variables.append(name_new_column)
        constants.CATEGORICAL_maxtypes.append(element[2])
    return [input_local, CONTROLS]

def adjust_get_dummies(input):
    input_local = input.copy()
    max_length = len(input)

    if constants.force_minimum_CATEGORICAL_types:

        existing_columns_categorical = {}
        for element in constants.CATEGORICAL_variables:
            existing_columns_categorical[element] = []
            column_to_search = input[element]
            for elem in column_to_search:
                if elem not in existing_columns_categorical[element]:
                    existing_columns_categorical[element].append(elem)

        n_added_rows = 0
        for i in range (0, len(constants.CATEGORICAL_variables)):
            maximum_types_local = 0
            aux = 0

            for element in constants.INTERACTIONS:
                compare_to = str(element[0]) + "*" + str(element[1])
                if constants.CATEGORICAL_variables[i] == compare_to:
                    aux = 1
                    break
            if aux:
                if constants.CATEGORICAL_maxtypes[i] > 2:
                    maximum_types_local = constants.CATEGORICAL_maxtypes[i]+1
            else:
                maximum_types_local = constants.CATEGORICAL_maxtypes[i]

            if(len(existing_columns_categorical[constants.CATEGORICAL_variables[i]])<maximum_types_local):
                #there are types not present, so we will add them and do the "get_dummies" method from Pandas library after
                for j in range(0, maximum_types_local):
                    if j not in existing_columns_categorical[constants.CATEGORICAL_variables[i]]:
                        '''to_add = input_local[1].copy()
                        to_add.at[CATEGORICAL_variables[i]] = j
                        input_local.append(to_add)'''

                        input_local = input_local.take([*range(len(input_local)), (len(input_local) -1)])
                        n_added_rows += 1
                        input_local.index = numpy.arange(0, len(input_local))
                        input_local._set_value( (constants.NUM_ROWS_TO_READ + n_added_rows -2) , constants.CATEGORICAL_variables[i], j)

    pre_output = pandas.get_dummies(data=input_local, columns=constants.CATEGORICAL_variables)
    output = pre_output[:max_length]
    return output

def adjust_for_control(input):
    input_local = input.copy()
    max_length = len(input)

    # deleting the first column of every categorical variable / interaction

    first_column_every_categorical = []
    for element in constants.CATEGORICAL_variables:
        for column_name in input_local.columns:
            if element in column_name:
                first_column_every_categorical.append(column_name)
                break

    for element in first_column_every_categorical:
        input_local.drop(element, axis="columns", inplace=True)

    # we still need to remove the columns with data for "constants.remove_from_CONTROLS" elements
    # thus we will search for every column with, e.g., "SEASON" in it, but that doesn't contain "*", because that will be an interaction

    for element in constants.remove_from_CONTROLS:
        for column in input_local.columns:
            if element in column and "*" not in column:
                input_local.drop(column, axis="columns", inplace=True)

    return input_local

def get_t_test(dv_input, idv_input):
    dv_positive_idv = []
    dv_negative_idv = []

    for i in range(0, len(dv_input)):
        if idv_input[i] == 1:
            dv_positive_idv.append(dv_input[i])
        else:
            dv_negative_idv.append(dv_input[i])

    return stats.ttest_ind(dv_positive_idv, dv_negative_idv)

def impact_idv(X, DV_local):
    IDV_positiveEffect_total = 0
    IDV_positiveEffect_numberElements = 0
    IDV_negativeEffect__total = 0
    IDV_negativeEffect__numberElements = 0

    for i in range(0, len(X)-1):
        if X[constants.IDV_COLUMN_NAME][i] == 1:
            IDV_positiveEffect_total += DV_local[i]
            IDV_positiveEffect_numberElements += 1
        else:
            IDV_negativeEffect__total += DV_local[i]
            IDV_negativeEffect__numberElements += 1

    if (IDV_positiveEffect_numberElements == 0 or IDV_negativeEffect__numberElements == 0):
        print("ERROR: Check the BINARY_CUTTINGPOINT, as no negative/positive values were found for binary IDV .")
        exit()

    average_positive = IDV_positiveEffect_total/IDV_positiveEffect_numberElements
    average_negative = IDV_negativeEffect__total/IDV_negativeEffect__numberElements

    return [average_positive, average_negative]

def idv_shuffler(input):
    random.seed(constants.SEED_RANDOM)
    input = numpy.array(input)
    randomize = numpy.arange(input.shape[0])
    random.shuffle(randomize)
    input = input[randomize]
    return input

def get_statistical_measurements(X2, Y2):
    # GET STATISTICAL MEASUREMENTS
    import statsmodels.api as sm

    X2 = sm.add_constant(X2)

    est = sm.OLS(Y2, X2)
    est2 = est.fit()

    file_output = open(constants.PATH_OUTPUT_TXT, "a+")
    file_output.write(str(est2.summary()))
    if constants.SCIENTIFIC_NOTATION:
        file_output.write("\n\nSTANDARD DEVIATION OF RESIDUAL VALUES AFTER FITTING {}.\n".format(numpy.std(est2.resid)))
    else:
        file_output.write("\n\nSTANDARD DEVIATION OF RESIDUAL VALUES AFTER FITTING {:.30f}.\n".format(numpy.std(est2.resid)))
    file_output.close()
    return numpy.std(est2.resid)

def simulate_after_randomization(X, IDV_randomized, regression_model, std_dv, keys, controls_column_names):
    predicted_DV = []
    for i in range(0, len(X)):
        X._set_value(i, constants.IDV_COLUMN_NAME, IDV_randomized[i])

        controls_partial = []
        for element in controls_column_names:
            controls_partial.append(X[element][i])

        DV_previous_partial = []
        for key in reversed(keys):
            DV_previous_partial.append(X[constants.DV_COLUMN_NAME + str(key)][i])

        parameters_to_predict = [ DV_previous_partial + [IDV_randomized[i]] + controls_partial ]

        predicted_value = regression_model.predict(parameters_to_predict)

        '''if(predicted_value > 100 or predicted_value < -100):
            return -1'''

        if constants.NOISE_ON_SIMULATIONS:
            #now we add noise to the prediction (normal distribution with 0 as mean, std_dv as spread of distribution)
            numpy.random.seed(constants.SEED_RANDOM)
            noise_to_add = numpy.random.normal(0, std_dv)
            predicted_value += noise_to_add

        for key in reversed(keys):
            if int(key) == -1:
                break
            X._set_value( i + 1, (constants.DV_COLUMN_NAME + str(key)), X[ (constants.DV_COLUMN_NAME + str( int(str(key)) + 1 )) ][i] )

        X._set_value( i + 1, (constants.DV_COLUMN_NAME + "-1"), predicted_value )
        predicted_DV.append(predicted_value)

    DV_output = pandas.Series((i[0] for i in predicted_DV))

    return [X, DV_output]

def write_results(CONTROLS, SEED_RANDOM_original, N_REPETITIONS_final, stopping_global, regression_model, t_test_before_MoTR, impact_beforeMoTR, sum_t_test_after_MoTR_stat, sum_t_test_after_MoTR_pvalue, sum_positive, sum_negative, sum_dif_impact):
    file_output = open(constants.PATH_OUTPUT_TXT, "a+")
    file_output.write("\nSTOPPING CONDITIONS:\n".format(constants.IDV_COLUMN_NAME, constants.DV_COLUMN_NAME))
    file_output.write("Maximum number of repetitions MoTR: {}.\n".format(constants.N_REPETITIONS))
    file_output.write("Minimum number of repetitions MoTR: {}.\n".format(constants.N_REPETITIONS_MIN))
    file_output.write("Stopping formula minimum value: {}.\n".format(constants.STOPPING_VALUE))
    file_output.write("------.\n\n".format(constants.STOPPING_VALUE))
    file_output.write("RESULTS: influence of {} on {}.\n".format( constants.IDV_COLUMN_NAME, constants.DV_COLUMN_NAME ))
    file_output.write("Source of data: {}.\n".format( constants.DATA_INPUT_CSV ))
    file_output.write("Number of rows used, since file start: {}.\n".format( constants.NUM_ROWS_TO_READ ))
    file_output.write("IDV column: {}.\n".format(constants.IDV_COLUMN_NAME))
    file_output.write("DV column: {}.\n".format(constants.DV_COLUMN_NAME))
    for element in constants.remove_from_CONTROLS:
        CONTROLS.remove(element)
    file_output.write("Control variables: {}.\n".format( CONTROLS ))
    file_output.write("Categorical variables: {}.\n".format(constants.CATEGORICAL_variables))
    file_output.write("Categorical maxtypes: {}.\n".format(constants.CATEGORICAL_maxtypes))
    file_output.write("Interactions ([name, name, maxtypes]): {}.\n".format(constants.INTERACTIONS))
    file_output.write("Number of lags (rows / days): {}.\n".format(constants.N_DAYS_LAG_ON_DV))
    file_output.write("Binary cutting point for the IDV: {}.\n".format( constants.BINARY_CUTTINGPOINT ))
    file_output.write("Initial seed for random method: {}.\n".format(SEED_RANDOM_original))
    file_output.write("Number of MoTR simulations: {}.\n".format(N_REPETITIONS_final))
    file_output.write("Noise on simulations: {}.\n".format(constants.NOISE_ON_SIMULATIONS))
    file_output.write("Save graph: {}.\n".format(constants.SAVE_GRAPH))
    file_output.write("Log 10 based DV: {}.\n".format(constants.LOG10_BASED))
    file_output.write("Scientific notation: {}.\n".format(constants.SCIENTIFIC_NOTATION))
    file_output.write("Save csv with simulated data: {}.\n".format(constants.SAVE_CSV_WITH_SIMULATED_DATA))
    file_output.write("(std / sqrt(i)) / mean = {}.\n".format(stopping_global))
    file_output.write("------.\n\n".format(constants.STOPPING_VALUE))

    if constants.SCIENTIFIC_NOTATION:
        file_output.write("\nCoefficient of IDV influence on DV: {}.\n".format( regression_model.coef_[1] ))
        file_output.write("\n--- BEFORE MoTR ---\n")
        file_output.write("T-test (stat), before MoTR: {}.\n".format(t_test_before_MoTR[0]))
        file_output.write("T-test (pvalue), before MoTR: {}.\n".format(t_test_before_MoTR[1]))
        file_output.write("Positive Effect of IDV on DV, before MoTR: {}.\n".format( impact_beforeMoTR[0] ))
        file_output.write("Negative Effect of IDV on DV, before MoTR: {}.\n".format( impact_beforeMoTR[1] ))
        file_output.write("Impact of IDV on DV, before MoTR: {}.\n".format( impact_beforeMoTR[0] - impact_beforeMoTR[1] ))
        file_output.write("\n--- AFTER MoTR ---\n")
        file_output.write("Avg. T-test (stat), after MoTR: {}.\n".format(numpy.average(sum_t_test_after_MoTR_stat)))
        file_output.write("Avg. T-test (pvalue), after MoTR: {}.\n".format(numpy.average(sum_t_test_after_MoTR_pvalue)))
        file_output.write("Avg. Positive Effect of IDV on DV, after MoTR: {}.\n".format( numpy.average(sum_positive) ))
        file_output.write("Avg. Negative Effect of IDV on DV, after MoTR: {}.\n".format( numpy.average(sum_negative) ))
        file_output.write("Avg. Impact of IDV on DV, after MoTR: {}.\n".format( numpy.average(sum_dif_impact) ))
    else:
        file_output.write("\nCoefficient of IDV influence on DV: {:.30f}.\n".format(regression_model.coef_[1]))
        file_output.write("\n--- BEFORE MoTR ---\n")
        file_output.write("T-test (stat), before MoTR: {:.30f}.\n".format(t_test_before_MoTR[0]))
        file_output.write("T-test (pvalue), before MoTR: {:.30f}.\n".format(t_test_before_MoTR[1]))
        file_output.write("Positive Effect of IDV on DV, before MoTR: {:.30f}.\n".format(impact_beforeMoTR[0]))
        file_output.write("Negative Effect of IDV on DV, before MoTR: {:.30f}.\n".format(impact_beforeMoTR[1]))
        file_output.write("Impact of IDV on DV, before MoTR: {:.30f}.\n".format(impact_beforeMoTR[0] - impact_beforeMoTR[1]))
        file_output.write("--- AFTER MoTR ---\n")
        file_output.write("Avg. T-test (stat), after MoTR: {:.30f}.\n".format(numpy.average(sum_t_test_after_MoTR_stat)))
        file_output.write("Avg. T-test (pvalue), after MoTR: {:.30f}.\n".format(numpy.average(sum_t_test_after_MoTR_pvalue)))
        file_output.write("Avg. Positive Effect of IDV on DV, after MoTR: {:.30f}.\n".format(numpy.average(sum_positive)))
        file_output.write("Avg. Negative Effect of IDV on DV, after MoTR: {:.30f}.\n".format(numpy.average(sum_negative)))
        file_output.write("Avg. Impact of IDV on DV, after MoTR: {:.30f}.\n".format(numpy.average(sum_dif_impact)))
    file_output.close()

def charts(show_legend, DV, impact_beforeMoTR, sum_positive, sum_negative, stopping_plot):
    plt.plot(DV)

    positive = [impact_beforeMoTR[0] for i in range(0, constants.NUM_ROWS_TO_READ - constants.N_DAYS_LAG_ON_DV)]
    negative = [impact_beforeMoTR[1] for i in range(0, constants.NUM_ROWS_TO_READ - constants.N_DAYS_LAG_ON_DV)]
    plt.plot(positive, color='green', label='POS effect before MoTR')
    plt.plot(negative, color='red', label='NEG effect before MoTR')

    positive_aft = [numpy.average(sum_positive) for i in range(0, constants.NUM_ROWS_TO_READ - constants.N_DAYS_LAG_ON_DV)]
    negative_aft = [numpy.average(sum_negative) for i in range(0, constants.NUM_ROWS_TO_READ - constants.N_DAYS_LAG_ON_DV)]
    plt.plot(positive_aft, color='green', linestyle='dotted', label='POS effect after MoTR')
    plt.plot(negative_aft, color='red', linestyle='dotted', label='NEG effect after MoTR')

    plt.xlabel('Day of study')
    ylabel = ""
    if constants.LOG10_BASED:
        ylabel = "log10 based " + constants.DV_COLUMN_NAME
    else:
        ylabel = constants.DV_COLUMN_NAME
    plt.ylabel(ylabel)
    plt.title("Impact of " + constants.IDV_COLUMN_NAME + " (>= " + str(constants.BINARY_CUTTINGPOINT) + ") on " + constants.DV_COLUMN_NAME)
    if show_legend:
        plt.legend()
    if constants.SAVE_GRAPH:
        plt.savefig(constants.PATH_OUTPUT_GRAPH, dpi=600)
    else:
        plt.show()

    plt.close()

    #plot the stopping condition
    partial = stopping_plot[int(constants.N_REPETITIONS_MIN/2):]
    plt.xlim([int(constants.N_REPETITIONS_MIN / 2), int(len(stopping_plot)-1)])
    plt.ylim([ 0, numpy.max(partial)*1.1 ])
    plt.plot(stopping_plot)
    plt.xlabel('Repetition of MoTR')
    plt.ylabel('(std/sqrt(i)) / mean)')
    plt.title("Evolution of the stopping condition over time")
    if constants.SAVE_GRAPH:
        plt.savefig(constants.PATH_OUTPUT_GRAPH_stopping, dpi=600)
    else:
        plt.show()

    plt.close()

def save_simulations_csv(sum_idv_dv_after_randomization, stopping_plot):
    # write all the simulations (idv and dv) to a csv file
    numpy.savetxt(constants.PATH_OUTPUT_CSV, sum_idv_dv_after_randomization, delimiter=", ", fmt='% s')
    numpy.savetxt(constants.PATH_OUTPUT_CSV_2, stopping_plot, delimiter=", ", fmt='% s')

def execution_time(start):
    file_output = open(constants.PATH_OUTPUT_TXT, "a+")
    file_output.write("\n\nExecution duration: %    seconds {}.\n\n".format((time.time() - start)))
    file_output.close()

def start():
    start_time = time.time()

    data_input = pandas.read_csv(constants.DATA_INPUT_CSV)

    data_input = data_input[0:constants.NUM_ROWS_TO_READ]

    DV = data_input[constants.DV_COLUMN_NAME]
    DV = remove_empty_cells(DV)

    DV_previous = get_DV_with_lags(DV)

    DV = DV.iloc[constants.N_DAYS_LAG_ON_DV:]

    IDV = data_input[constants.IDV_COLUMN_NAME][constants.N_DAYS_LAG_ON_DV: constants.NUM_ROWS_TO_READ]
    IDV = remove_empty_cells(IDV)

    idv_to_binary(IDV)

    DV.index = numpy.arange(0, len(DV))  # to make sure the index is the same on X and Y (DV)
    IDV.index = numpy.arange(0, len(IDV))

    IDV_original = IDV.copy()

    CONTROLS = set_Controls_list()

    keys = []
    if constants.LOG10_BASED:
        DV = numpy.log10(DV)
        for key in DV_previous.keys():
            DV_previous[key] = numpy.log10(DV_previous[key])
            keys.append(key)

    DV_previous_for_DataFrame = get_DV_previous_for_DataFrame(DV_previous, keys)

    columns_for_DataFrame = DV_previous_for_DataFrame[0] + [constants.IDV_COLUMN_NAME] + CONTROLS
    controls_data = get_control_values(data_input, CONTROLS)
    data_for_DataFrame = DV_previous_for_DataFrame[1] + [IDV] + controls_data

    X = pandas.DataFrame(list(zip(*data_for_DataFrame)), columns=columns_for_DataFrame)

    data_interactions = generate_data_interactions(X)

    X = data_interactions[0]
    new_CONTROLS = data_interactions[1]
    if new_CONTROLS != 0:
        CONTROLS = data_interactions[1]

    X = adjust_get_dummies(X)
    X = adjust_for_control(X)

    X_original = X.copy()

    Y = DV.copy()

    t_test_before_MoTR = get_t_test(DV, IDV)
    impact_beforeMoTR = impact_idv(X, DV)

    regression_model = linear_model.LinearRegression(fit_intercept=False)
    regression_model.fit(X.values, Y)

    # this std_dv of the residuals after fitting will be the maximum noise to randomly add to the predicted values
    std_dv_residuals = get_statistical_measurements(X.copy(), Y.copy())

    # sum_idv_dv_after_randomization will store all the simulations (each pair of columns are IDV and DV of 1 iteration)
    sum_idv_dv_after_randomization = []

    sum_t_test_after_MoTR_stat = []
    sum_t_test_after_MoTR_pvalue = []
    sum_dif_impact = []
    sum_positive = []
    sum_negative = []
    stopping_plot = []

    controls_column_names = []
    for element in CONTROLS:
        if element in constants.CATEGORICAL_variables:
            controls_column_names += [col for col in X if col.startswith(element)]
        else:
            controls_column_names.append(element)

    stopping_global = 0
    skipped_loops = 0

    N_REPETITIONS_end = 0
    SEED_RANDOM_original = constants.SEED_RANDOM

    for i in range(constants.SEED_RANDOM, constants.N_REPETITIONS + constants.SEED_RANDOM):
        constants.SEED_RANDOM = i

        X_local = X_original.copy()
        IDV_local = IDV_original.copy()

        IDV_local = idv_shuffler(IDV_local)

        simulation_output = simulate_after_randomization(X_local, IDV_local, regression_model, std_dv_residuals, keys, controls_column_names)

        if (simulation_output == -1):
            print("skipped")
            skipped_loops += 1
            continue

        t_test_after_MoTR = get_t_test(simulation_output[1], simulation_output[0][constants.IDV_COLUMN_NAME])
        impact_afterMoTR = impact_idv(simulation_output[0], simulation_output[1])

        sum_idv_dv_after_randomization.append(IDV_local.tolist())
        sum_idv_dv_after_randomization.append(simulation_output[1].tolist())

        sum_t_test_after_MoTR_stat.append(t_test_after_MoTR[0])
        sum_t_test_after_MoTR_pvalue.append(t_test_after_MoTR[1])

        sum_dif_impact.append((impact_afterMoTR[0] - impact_afterMoTR[1]))
        sum_positive.append(impact_afterMoTR[0])
        sum_negative.append(impact_afterMoTR[1])

        stopping = (numpy.std(sum_dif_impact) / numpy.sqrt(i)) / numpy.abs(numpy.average(sum_dif_impact))
        stopping_plot.append(stopping)

        N_REPETITIONS_end += 1

        if stopping < constants.STOPPING_VALUE and i > constants.N_REPETITIONS_MIN:
            stopping_global = stopping
            break

        print("\rDone " + str(i) + " / " + str(constants.N_REPETITIONS) + " (skipped: " + str(
            skipped_loops) + " times) - (std/sqrt(i))/mean = " + str(stopping), end='', flush=True)

    constants.SEED_RANDOM = SEED_RANDOM_original

    write_results(CONTROLS, SEED_RANDOM_original, N_REPETITIONS_end, stopping_global, regression_model, t_test_before_MoTR, impact_beforeMoTR, sum_t_test_after_MoTR_stat, sum_t_test_after_MoTR_pvalue, sum_positive, sum_negative, sum_dif_impact)

    if constants.SAVE_CSV_WITH_SIMULATED_DATA:
        save_simulations_csv(sum_idv_dv_after_randomization, stopping_plot)

    charts(True, DV, impact_beforeMoTR, sum_positive, sum_negative, stopping_plot)

    execution_time(start_time)

constants()

for element in constants.LAGS_TO_SIMULATE:
    constants()
    constants.N_DAYS_LAG_ON_DV = element
    update_constants()
    print("\nSTARTING simulation with LAG = " + str(element) + " days/rows.")
    start()
    print("\nFINISHED this simulation.\n")
print("\nEND OF SCRIPT.")
