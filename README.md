# 1 - MoTR-python v1 (Linear Regression)
Python implementation of the Model-Twin Randomization (MoTR) method.
This **first version** applies linear regression. Future versions will be developed and published.

This is a **public repository** to complement the following conference paper. 

**Igor Matias, Eric J. Daza, and Katarzyna Wac. "What possibly affects nighttime heart rate? Conclusions from N-of-1 observational data", SAGE Digital Health, 24 August 2022.**
https://doi.org/10.1177%2F20552076221120725 (version of the repository used in that paper was the commit 9ca96d2f7e92d8e9f4e605951a91d92335c1bd81)

Check the "MoTR.py" file for the full Python implementation.

# 2 - What is MoTR?

MoTR ("motor") is a new causal inference method that artificially emulates an n-of-1 randomized trial (i.e., the gold standard due to randomization) from the n-of-1 observational study dataset. 
It does so by first modeling the outcome of interest as a function of the exposure of interest, along with an individual's assumed recurring confounders (i.e., daily observed variables thought to influence or affect both the exposure and the outcome). 
MoTR then randomly shuffles (i.e., permutes) the exposures, which were originally only observed, thereby simulating an n-of-1 randomized trial. 
This allows us to infer more accurately a suggested effect of daily stressors beyond just correlation.

# 3 - Applicability of the method

The MoTR method is presented as a new tool to assess potential causality using intensive longitudinal data.
One use case of the MoTR method is to help develop n-of-1 randomized trials for diagnosis or intervention/treatment. 
After applying it to data, researchers can use this method to select findings with the largest statistically discernible differences from the naive. 
That will indicate possible confounding variables that can change decisions on a future intervention. 
Finally, MoTR can also help discuss a possible intervention plan by analyzing the highest potential causality between intensive longitudinal data and the outcome expected to be affected during a study.
Thus, this novel method is intended to be used with one's data. 
However, the best practice would be to (a) use occasional self-screening mental health questionnaires and (b) work with a health expert to analyze the data and conclude about them.

# 4 - How to use the script (as easy as ABC)?

### A - Check your data
After gathering data, you must ensure the exposure of interest, outcome of interest, and measurements of possible confounders/controls (e.g., day of the week, month, season) are together in a single .csv file. 
Each column must represent a data type, with a row for each observed value. 
All the columns should have the same number of rows (i.e., when values are missing you should create an empty space).

### B - Personalize the script parameters
All you will need to do is to personalize the parameters between lines 14 and 56 (including) of the "MoTR.py" file.
You should not modify any other lines to maintain the integrity and validity of the results you will obtain.

The content of each parameter is as follows (ordered by line number).

1. DATA_INPUT_CSV - Specify the path to the .csv with the data to be used (starting point is the location of this script).

2. NUM_ROWS_TO_READ - Number of total rows you want to get from the .csv file (top to bottom).

3. IDV_COLUMN_NAME - The column's title with data referring to the exposure of interest (independent variable) must match the name in the first row of that column.

4. DV_COLUMN_NAME - The column's title with data referring to the outcome of interest (dependent variable) must match the name in the first row of that column.

5. TOTAL_columns - DO NOT EDIT this.

6. remove_from_CONTROLS - Column names you want to exclude from the controls (leave empty if none).

7. CATEGORICAL_variables - Column names to be treated as containing categorical data (will be one-hot encoded)—all the interactions below will be automatically added here.

8. force_minimum_CATEGORICAL_types - Set to True if you want to make sure you have a specific number of possibilities while one-hot encoding a specific categorical column 
(e.g., if you have a dataset with only Male/Female gender but want to include the possibility for data with "Other," you would set this to True).

9. CATEGORICAL_maxtypes - Set the number of maximum possibilities for each of the CATEGORICAL_variables above (separated by a comma and in order with the variables above).

10. INTERACTIONS - Of type "[name, name, maxtypes]" where at least one of the columns should have binary values, and any non-binary column must be categorical.

11. LAGS_TO_SIMULATE - List of lags to be simulated (will repeat the entire script for each, e.g., "[1, 3, 6]" will give you results with lags of 1, 3, and 6 rows of data).

12. N_DAYS_LAG_ON_DV - DO NOT EDIT this.

13. BINARY_CUTTINGPOINT - Your exposure of interest will be transformed into a binary data set, i.e., all its rows will be 0 or 1 depending on whether the value is below this BINARY_CUTTINGPOINT or above, respectively.

14. N_REPETITIONS - Maximum number of repetitions of the simulation of data (including shuffling the exposure of interest) for each lag defined above.

15. N_REPETITIONS_MIN - Minimum number of repetitions.

16. STOPPING_VALUE - The shuffling of exposure and simulation of data will stop whenever the stopping condition referred to in the article (see lines 575 and 580 of the script) is met. This STOPPING_VALUE is taken into account.

17. SEED_RANDOM - Set this to a value that will be used to start the random seed for all the simulations. This will enable the replicability of your results.

18. NOISE_ON_SIMULATIONS - Set to True if random noise should be added to the simulations (follows a maximum level—lines 327 to 331).

19. SAVE_GRAPH - Set to True to export the graphs at the end of the simulations for each lag specified above.

20. LOG10_BASED - Set to True if the outcome of interest's data should be logarithmically scaled.

21. SCIENTIFIC_NOTATION - Set to True if the final written exported report file should represent big numbers in scientific notation.

22. SAVE_CSV_WITH_SIMULATED_DATA - Set to True to save a file with all the simulated data after the simulations.

23. FOLDER_OUTPUT - Specify the path where all the outputs should be saved.

24. PREFIX_NAMES_OUTPUT - Edit this if you want to have all the exported files starting with a specific code.

25. NAME_OUTPUT_TXT - Specify the name of the written report .txt file (will start with the PREFIX_NAMES_OUTPUT if applicable).

26. NAME_OUTPUT_GRAPH - Specify the name of the result's graph (will start with the PREFIX_NAMES_OUTPUT if applicable).

27. NAME_OUTPUT_GRAPH_stopping - Specify the name of the stopping condition evolution's graph (will start with the PREFIX_NAMES_OUTPUT if applicable).

28. NAME_OUTPUT_CSV - Specify the file name that will save all the simulated data (will start with the PREFIX_NAMES_OUTPUT if applicable).

29. NAME_OUTPUT_CSV_2 - Specify the file name that will save all the stopping condition values during the script execution (will start with the PREFIX_NAMES_OUTPUT if applicable).

### C - Run it and wait :)
Once all the parameters have been set, you just have to run the script and wait for its end.
After its completion, you will have access to all the exported files and graphs in the "FOLDER_OUTPUT" defined.

# 5 - Disclaimer

This script is provided as is and should not be used to infer self-medication and/or life-threatening self-intervention.

# ---

We, IM, EJD, and KW hope you find this script a novel MoTR for your research and/or personal curiosity. 
If you would like to add, correct, or improve something, please go ahead and send a pull request.

# ---
